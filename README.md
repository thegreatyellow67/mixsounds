# mixsounds v.20210501

## Description

Choose one or more ambient background sounds and this script will play and mix them for you!

## Dependencies

whiptail, ffmpeg (ffplay)

## Usage

```
mixsounds play
mixsounds stop
mixsounds -h (displays this help)
```

Before run the script, put the folder **.mixsounds** with sound files in your $HOME path. If you want to put sound files in different folder and path, remember to change value of variable **mixsounds_flr** inside the script.

Example:

`
mixsounds_flr=/home/user/.config/mixsounds
`

then, copy the script in a valid path, for example **/usr/local/bin**

Furthermore, the names of the files contained in the sound folder must not contain spaces, use the underscore character instead.
